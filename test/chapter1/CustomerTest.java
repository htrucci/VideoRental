package chapter1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class CustomerTest {

	private static final int RENTED_0 = 0;
	private static final int RENTED_3 = 3;
	private static final int RENTED_4 = 4;

	@Test
	public void countNewReleaseMovie_when_customer_has_no_rental() {
		Customer customer = new Customer("dd");
		List<Rental> rentals = new ArrayList<Rental>();
		int newReleaseMovieCount = customer.countNewReleaseMovie(rentals);
		assertEquals(0, newReleaseMovieCount);

	}

	@Test
	public void 고객이_아무것도_빌리지_않을때_대여출력() {
		Customer customer = new Customer("NOT_IMPORTANT");
		assertEquals("Rental Record for NOT_IMPORTANT\n" + "Amount owed is 0.0\n" + "You earned 0frequent renter points0",
				customer.statement());
	}

	@Test
	public void 고객이_레귤러_영화를_빌렸을때_대여출력() {
		int rented0 = RENTED_0;
		Customer customer = addRegularMovieWithRentDays(rented0);
		assertEquals("Rental Record for NOT_IMPORTANT\n" + "	TITLE_IS_NOT_IMPORTANT	2.0\n" + "Amount owed is 2.0\n"
				+ "You earned 1frequent renter points0", customer.statement());
	}

	@Test
	public void 고객이_레귤러_영화를_삼일이상_빌렸을때_대여출력() {
		int rented3 = RENTED_3;
		Customer customer = addRegularMovieWithRentDays(rented3);
		assertEquals("Rental Record for NOT_IMPORTANT\n" + "	TITLE_IS_NOT_IMPORTANT	3.5\n" + "Amount owed is 3.5\n"
				+ "You earned 1frequent renter points0", customer.statement());
	}

	private Customer addRegularMovieWithRentDays(int rented3) {
		Customer customer = new Customer("NOT_IMPORTANT");
		String title = "TITLE_IS_NOT_IMPORTANT";
		Movie movie = new Movie(title, Movie.REGULAR);
		Rental rental = new Rental(movie, rented3);
		customer.addRental(rental);
		return customer;
	}

	@Test
	public void 고객이_릴리즈_영화를_빌렸을때_대여출력() {
		int rented0 = RENTED_0;
		Customer customer = addReleaseMovieWithRentDays(rented0);
		assertEquals("Rental Record for NOT_IMPORTANT\n"+"	TITLE_IS_NOT_IMPORTANT	0.0\n"+
"Amount owed is 0.0\n"+
"You earned 1frequent renter points0", customer.statement());
	}

	private Customer addReleaseMovieWithRentDays(int rented0) {
		Customer customer = new Customer("NOT_IMPORTANT");
		String title = "TITLE_IS_NOT_IMPORTANT";
		Movie movie = new Movie(title, Movie.NEW_RELEASE);
		Rental rental = new Rental(movie, rented0);
		customer.addRental(rental);
		return customer;
	}
	@Test
	public void 고객이_릴리즈_영화를_이틀_이상_빌렸을때_대여출력() {
		int rented3 = RENTED_3;
		Customer customer = addReleaseMovieWithRentDays(rented3);
		assertEquals("Rental Record for NOT_IMPORTANT\n" + "	TITLE_IS_NOT_IMPORTANT	9.0\n" + "Amount owed is 9.0\n"
				+ "You earned 2frequent renter points0", customer.statement());
	}

	@Test
	public void 고객이_칠드런_영화를_사일_이상_빌렸을때_대여출력() {
		int rented4 = RENTED_4;
		Customer customer = addChildrenMovieWithRentDays(rented4);
		assertEquals("Rental Record for NOT_IMPORTANT\n"+
"	TITLE_IS_NOT_IMPORTANT	3.0\n"+
"Amount owed is 3.0\n"+
"You earned 1frequent renter points0", customer.statement());
	}

	private Customer addChildrenMovieWithRentDays(int rented4) {
		Customer customer = new Customer("NOT_IMPORTANT");
		String title = "TITLE_IS_NOT_IMPORTANT";
		Movie movie = new Movie(title, Movie.CHILDREN);
		Rental rental = new Rental(movie, rented4);
		customer.addRental(rental);
		return customer;
	}
}
