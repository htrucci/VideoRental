package chapter1;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class Customer {

	private String name;
	private List<Rental> rentals = new ArrayList<>();

	public Customer(String name) {
		this.name = name;
	}

	public void addRental(Rental rental) {
		rentals.add(rental);
	}

	public String getName() {
		return name;
	}

	public String statement() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;

		Iterator<Rental> iterator = rentals.iterator();
		String result = "Rental Record for " + getName() + "\n";

		while (iterator.hasNext() ) {
			double thisAmount = 0;
			Rental rental = (Rental) iterator.next();
			thisAmount = priceFor(rental, thisAmount);
			frequentRenterPoints++;
			frequentRenterPoints = addPointForReleaseTwoDayRented(frequentRenterPoints, rental);

			// 이 대여에 대한 요금 계산 결과 표시
			result += "\t" + rental.getMovie().getTitle() + "\t" + String.valueOf(thisAmount) + "\n";
			totalAmount += thisAmount;
		}

		// footer 추가
		result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
		result += "You earned " + String.valueOf(frequentRenterPoints) + "frequent renter points";
		result += countNewReleaseMovie(rentals);
		return result;
	}

	private int addPointForReleaseTwoDayRented(int frequentRenterPoints, Rental each) {
		if ((each.getMovie().getPriceCode() == Movie.NEW_RELEASE) && each.getDaysRented() > 1)
			frequentRenterPoints++;
		return frequentRenterPoints;
	}

	private double priceFor(Rental each, double thisAmount) {
		switch (each.getMovie().getPriceCode()) {
		case Movie.REGULAR:
			thisAmount += 2;
			if (each.getDaysRented() > 2)
				thisAmount += (each.getDaysRented() - 2) * 1.5;
			break;
		case Movie.NEW_RELEASE:
			thisAmount += each.getDaysRented() * 3;
			break;
		case Movie.CHILDREN:
			thisAmount += 1.5;
			if (each.getDaysRented() > 3)
				thisAmount += (each.getDaysRented() - 3) * 1.5;
			break;
		}
		return thisAmount;
	}

	protected int countNewReleaseMovie(List<Rental> rentals) {
		return 0;
	}
}